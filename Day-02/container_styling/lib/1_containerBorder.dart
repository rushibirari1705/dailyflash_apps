

/*
Create a screen that displays the container in the center having size (height:
200, width: 200). The Container must have border with rounded edges. The
border must be of the color red. Display a Text in the center of the container.
*/
import 'package:flutter/material.dart';

class ContainerBorder extends StatefulWidget{
  const ContainerBorder({super.key});

  @override
  State<ContainerBorder> createState() => _ContainerBorder();

}

class _ContainerBorder extends State<ContainerBorder>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Container Border Style",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
        ),
        backgroundColor: Colors.lightBlue,
      ),

      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration:BoxDecoration(
            color: const Color.fromARGB(255, 255, 222, 238),
            border: Border.all(
              color: Colors.red,
              width: 5,
            ),
          ),
          alignment: Alignment.center,
          child: const Text(" Hello World!!",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );

  }
}