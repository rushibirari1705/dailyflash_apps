

/*
In the screen add a container of size( width 100, height: 100) that must only
have a left border of width 5 and color as per your choice. Give padding to the
container and display a text in the Container.
 */
import 'package:flutter/material.dart';

class ContainerBorder extends StatefulWidget{
  const ContainerBorder({super.key});

  @override
  State<ContainerBorder> createState() => _ContainerBorder();

}

class _ContainerBorder extends State<ContainerBorder>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Container Border Style",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
        ),
        backgroundColor: Colors.lightBlue,
      ),

      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration:const BoxDecoration(
            color:  Color.fromARGB(255, 255, 222, 238),
            border: Border(
            left: BorderSide(width: 5.0, color:  Color.fromARGB(255, 0, 0, 0)),
          ),
          ),
          alignment: Alignment.center,
          child: const Text(" Hello World!!",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );

  }
}