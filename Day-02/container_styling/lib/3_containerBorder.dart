
/*
In the screen add a container of size( width 100, height: 100) . The container
must have a border as displayed in the below image. Give color to the container
and border as per your choice.
*/
import 'package:flutter/material.dart';

class ContainerBorder extends StatefulWidget{
  const ContainerBorder({super.key});

  @override
  State<ContainerBorder> createState() => _ContainerBorder();

}

class _ContainerBorder extends State<ContainerBorder>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Container Border Style",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
        ),
        backgroundColor: Colors.lightBlue,
      ),

      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            color:  const Color.fromARGB(255, 255, 222, 238),
            border: Border.all(
              width: 4,
              color: const Color.fromARGB(255, 255, 128, 119),
            ),
            borderRadius: const BorderRadius.only(topRight: Radius.circular(50)),
          ),
          alignment: Alignment.center,
          child: const Text(" Hello World!!",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );

  }
}