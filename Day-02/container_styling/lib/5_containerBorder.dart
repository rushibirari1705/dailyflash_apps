

/*
Add a container with the color red and display the text "Click me!" in the center
of the container. On tapping the container, the text must change to “Container
Tapped” and the color of the container must change to blue.
*/
import 'package:flutter/material.dart';

class ContainerBorder extends StatefulWidget{
  const ContainerBorder({super.key});

  @override
  State<ContainerBorder> createState() => _ContainerBorder();

}

class _ContainerBorder extends State<ContainerBorder>{

  bool boxColor = false;
  String  text1 = "Click me";

  void changeColor(){
    setState(() {
      boxColor = true;
      text1 = "Container Tapped";
      
    });
    
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Container Border Style",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
        ),
        backgroundColor: Colors.lightBlue,
      ),

      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration:BoxDecoration(
            color: boxColor ? Colors.blue : Colors.red,
            border: Border.all(
              color: const Color.fromARGB(255, 48, 48, 48),
              width: 5,
            ),
          ),
          alignment: Alignment.center,
          child: TextButton(
            onPressed: changeColor,
            child: Text(text1, style: const TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold
              ),
            ),
          ),
            ),
          ),
        );
      

  }
}