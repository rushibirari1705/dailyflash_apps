
import "package:flutter/material.dart";
//import '1_containerBorder.dart';
//import '2_containerBorder.dart';
//import '3_containerBorder.dart';
//import '4_containerBorder.dart';
import '5_containerBorder.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home: ContainerBorder(),
      debugShowCheckedModeBanner: false,
    );
  }
}